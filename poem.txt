
Little leaves fall softly down
Red and orange, yellow and brown
Whirling, twirling round and round
Falling softly to the ground.

Little leaves fall softly down
To make a carpet on the ground.
Then, swish, the wind comes whistling by
And sends them dancing to the sky.
